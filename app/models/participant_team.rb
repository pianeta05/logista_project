# == Schema Information
#
# Table name: participant_teams
#
#  id             :integer          not null, primary key
#  participant_id :integer
#  team_id        :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  leader         :boolean
#

class ParticipantTeam < ApplicationRecord
  validates :participant_id, uniqueness: {scope: :team_id}
  belongs_to :participant
  belongs_to :team

  scope :leaders, -> { where(leader: true) }
  scope :non_leaders, -> { where(leader: false) }

end
