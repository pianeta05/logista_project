# == Schema Information
#
# Table name: ngos
#
#  id         :integer          not null, primary key
#  name       :string
#  country    :string
#  image      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Ngo < ApplicationRecord
  validates :name, presence: true, length: {maximum: 200}
  validates :country, presence: true, length: {maximum: 200}
  has_many :ngo_users, dependent: :destroy
  has_many :users, through: :ngo_users
  has_many :requests
  has_many :projects

  scope :without_user, ->(user_id) { where.not(id: NgoUser.where(user_id: user_id).pluck(:ngo_id))}
  scope :with_admin, -> (user_id) { where(id: NgoUser.where(user_id: user_id, admin: true).pluck(:ngo_id))}
  mount_uploader :image, NgoImageUploader

  def admins
    User.where(id: NgoUser.where(ngo_id: self.id, admin: true).pluck(:user_id))
  end

  def workers
    User.where(id: NgoUser.where(ngo_id: self.id, admin: false).pluck(:user_id))
  end

  def is_admin?(user)
    admins.pluck(:id).include?(user.id)
  end

  def remove_user(user_id)
    NgoUser.where(user_id: user_id, ngo_id:self.id).destroy_all
  end

  def add_admin(user)
    if user
      if users.include?(user)
        NgoUser.where(ngo_id: id, user_id: user.id).update(admin: true)
      else
        NgoUser.create(ngo_id: id, user_id: user.id, admin: true)
      end
    end
  end

  def add_worker(user)
    if user
      if users.include?(user)
        NgoUser.where(ngo_id:self.id, user_id: user.id).update(admin: false)
      else
        NgoUser.create(user_id: user.id, ngo_id:self.id, admin: false)
      end
    end
  end

  def last_admin
    NgoUser.where(ngo_id:self, admin:true).count==1
  end

  def my_ngos(user_id)
    Ngo.find(NgoUser.where(user_id:user_id).pluck(:ngo_id))
  end
end
