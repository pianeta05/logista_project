# == Schema Information
#
# Table name: participants
#
#  id            :integer          not null, primary key
#  name          :string
#  nationality   :string
#  residence     :string
#  telephone     :integer
#  email         :string
#  date_of_birth :date
#  project_id    :integer
#  team_id       :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  sex           :string
#  country_code  :string
#

class Participant < ApplicationRecord

  before_save :titleize_params

  validates :name, presence: true, length: {maximum: 200}
  validates :nationality, presence: true, length: {maximum: 100}
  validates :residence, presence: true, length: {maximum: 100}
  validates :email, presence: true, length: {maximum: 100}, uniqueness: true
  validates :date_of_birth, presence: true
  validates :sex, presence: true,  inclusion: { in: %w(Male Female Other),
    message: "%{value} is not a valid sex" }
  validate :is_older_than_13

  has_many :participant_teams, dependent: :destroy
  has_many :teams, through: :participant_teams

  scope :in_ngo, -> (ngo) {Participant.find(ParticipantTeam.where(team_id:Team.where(project_id:(Ngo.find(ngo)).projects.pluck(:id)).pluck(:id)).pluck(:participant_id))}
  def age
    age = Date.today.year - date_of_birth.year
    age -= 1 if Date.today < date_of_birth + age.years #for days before birthday
    return age
  end

  def is_older_than_13
    if date_of_birth.present? && ((date_of_birth + 13.years) >= Date.today)
      errors.add(:date_of_birth, 'You should be over 13 years old.')
    end
  end

  def join(team, options = {leader: false})
    unless ParticipantTeam.where(participant_id: id, team_id: team.id).count > 0
      ParticipantTeam.create(participant_id: id, team_id: team.id, leader: options[:leader])
    end
  end

  # NGOs related to the participant through some project
  def ngos
    Ngo.where(id: Project.where(id: teams.pluck(:project_id)).pluck(:ngo_id))
  end

  def can_edit?(user)
    Project.find_by(id:self.teams.pluck(:project_id)).can_edit?(user)
  end

  #projects in whitch the participant is
  def projects
    Project.find_by(id: self.teams.pluck(:project_id))
  end


  private

    def titleize_params
      self.name = self.name.titleize
      self.nationality = self.nationality.titleize
      self.residence = self.residence.titleize
    end
end
