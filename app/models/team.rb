# == Schema Information
#
# Table name: teams
#
#  id         :integer          not null, primary key
#  country    :string
#  budget     :float
#  project_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Team < ApplicationRecord

  before_save :titleize_params

  validates :country, presence: true, length: {maximum: 100}, uniqueness: { scope: :project_id,
  message: "should be unique in a project" }
  validates :budget, presence: true, numericality:  { greater_than_or_equal_to: 0, message: "No puede ser negativo" }
  validates :project_id, presence: true, length: {maximum: 50}

  belongs_to :project
  has_many :participant_teams
  has_many :participants, through: :participant_teams

  def add_participant(participant, leader = false)
    unless ParticipantTeam.where(participant: participant, team: self).count > 0
      ParticipantTeam.create(participant: participant, team: self, leader: leader)
    end
  end

  def can_edit?(user)
    project.can_edit?(user)
  end

  def leaders
    Participant.where(id: participant_teams.leaders.pluck(:participant_id))
  end

  def non_leaders
    Participant.where(id: participant_teams.non_leaders.pluck(:participant_id))
  end

  def eligible_participants
    Participant.where(residence: self.country).or(Participant.where(nationality: self.country)).where.not(id: self.participants.pluck(:id))
  end

  def ordered_participants
    Participant.where(id: ParticipantTeam.where(team_id:self.id).order("leader DESC").pluck(:participant_id))
  end

  def make_leader(participant)
    ParticipantTeam.find_by(team_id: self.id, participant_id: participant.id).update(leader: true)
  end

  def make_regular(participant)
    ParticipantTeam.find_by(team_id: self.id, participant_id: participant.id).update(leader: false)
  end
  private

    def titleize_params
      self.country = self.country.titleize
    end

end
