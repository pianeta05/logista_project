json.extract! ngo, :id, :name, :country, :image, :created_at, :updated_at
json.url ngo_url(ngo, format: :json)
