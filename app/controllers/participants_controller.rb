class ParticipantsController < ApplicationController
  before_action :set_participant, only: [:show, :edit, :update, :destroy]
  before_action :set_breadcrumbs
  before_action :set_ngo
  before_action :set_team, only: [:new, :create]
  before_action :authenticate_user!
  before_action :can_edit?, only: [:show, :edit]

  # GET /participants
  def index
    @participants = Participant.in_ngo(@ngo.id)
    @breadcrumbs << { text: t(".index.title")}
  end

  # GET /participants/1
  def show
    @breadcrumbs << { text: @participant.name, path: ngo_participant_path(@ngo.id, @participant) }
  end

  # GET /participants/new
  def new
    @participant = Participant.new
    @participant.nationality = @team.country
    @participant.residence = @team.country
    @participant.team_id = @team.id
    @participant.project_id = @team.project.id
    @breadcrumbs << {text: t(".breadcrumb_new"), path: ngo_new_participant_path(@ngo)}
  end

  # GET /participants/1/edit
  def edit
    @breadcrumbs << {text: @participant.name, path: ngo_participant_path(@ngo, @participant)}
    @breadcrumbs << {text: t(".breadcrumb_edit"), path: edit_ngo_participant_path(@ngo.id)}
  end

  # POST /participants
  def create
    @participant = Participant.new(participant_params)
    if @participant.save
      @participant.reload
      @participant.join(@team)
      flash[:success] = t(".join.success")
      redirect_to @team
    else
      redirect_to ngo_new_participant_path(@ngo)
      # render :new
    end
  end

  # PATCH/PUT /participants/1
  def update
    if @participant.update(participant_params)
      redirect_to ngo_participant_path(@ngo, @participant), notice: t(".success")
    else
      redirect_to ngo_participant_path(@ngo, @participant), error:t(".error")
      # render :edit
    end
  end


  # DELETE /participants/1
  def destroy
    @participant.destroy
    redirect_to ngo_participants_path(@ngo), flash: {success: t(".success")}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_participant
      @participant = Participant.find(params[:id])
    end

    def set_ngo
      @ngo = Ngo.find(params[:ngo_id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def participant_params
      params.require(:participant).permit(:name, :nationality, :residence, :country_code, :telephone, :email, :date_of_birth, :sex, :project_id, :team_id)
    end

    def set_breadcrumbs
      set_ngo
      @breadcrumbs =
      [
        { text: t(".set_breadcrumbs.index"), path: root_path },
        { text: @ngo.name, path: ngo_path(@ngo) },
        # { text: t(".set_breadcrumbs.title"), path: ngo_participants_path(@ngo) }
      ]
    end

    def set_team
      @team = params[:team_id].blank? ? nil : Team.find(params[:team_id])
      if(@team && @team.can_edit?(current_user))
        @breadcrumbs << { text: @team.project.name, path: project_path(@team.project) }
        @breadcrumbs << { text: @team.country, path: team_path(@team) }
      else
        redirect_to root_path, notice: "You're not authorized"
      end
    end

    def can_edit?
      unless @participant.can_edit?(current_user)
        redirect_to root_path, notice: "You're not authorized"
      end
    end

end
