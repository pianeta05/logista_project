class RequestsController < ApplicationController

  before_action :set_request, only: [:show, :edit, :update, :destroy]
  before_action :set_ngo, only: [:create]
  before_action :authenticate_user!

  def index
    @requests= Request.to_user(current_user.id)
  end

  # GET /Requests/1
  # GET /Requests/1.json
  def show
  end

  # GET /requests/1/edit
  def edit
  end

  # POST /requests
  # POST /requests.json
  def create
    @request = Request.create(user_id: current_user.id, ngo_id:@ngo.id)
    if @request.save
      redirect_to join_ngos_path, notice: t('.success')
    else
      redirect_to join_ngos_path, notice: t('.error')
    end
  end

  # PATCH/PUT /requests/1
  # PATCH/PUT /requests/1.json
  def update
    respond_to do |format|
      if @query.update(request_params)
        format.html { redirect_to @query, notice: t('.success') }
        format.json { render :show, status: :ok, location: @query }
      else
        format.html { render :edit }
        format.json { render json: @query.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /requests/1
  # DELETE /requests/1.json
  def destroy
    @query.destroy
    respond_to do |format|
      format.html { redirect_to requests_url, notice: t('.success') }
      format.json { head :no_content }
    end
  end

  def handle
    accept= params[:accept]
    request= Request.find(params[:request_id])
    if accept == "true"
      NgoUser.create(user_id: request.user.id, ngo_id: request.ngo.id, admin: false)
    end
    request.destroy
    redirect_to requests_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_request
      @query = Request.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def request_params
      params.require(:request).permit()
    end

    def set_ngo
      @ngo = Ngo.find(params[:ngo_id])
    end
end
