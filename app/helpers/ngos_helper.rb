module NgosHelper
  def ngo_avatar ngo
    if ngo.image.present?
      image_tag ngo.image_url :thumbnail
    else
      image_tag 'images.jpg'
    end
  end
end
