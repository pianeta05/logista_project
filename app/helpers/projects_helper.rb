module ProjectsHelper
  def categories_for_select
    ['KA1', 'KA2','KA3']
  end

  def project_lists
    ['all', 'current', 'future', 'past']
  end
end
