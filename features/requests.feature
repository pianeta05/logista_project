Feature: request
I should be able to join to a ngos in the app

Background:
  Given I am a registered user
  And I sign in
  And There are several ngos

  @javascript
  Scenario: Joinig to a ngo
    When I visit the ngos page
    And I join to a NGO
    Then I should see a Requested button

  @javascript
  Scenario: Showing requests
    Given I am a user administrator of an NGO
    Given There are requests for my NGO
    When I visit the request page
    Then I should see all my request for other users

  Scenario: Accepting a user request
    Given I am a user administrator of an NGO
    Given There are requests for my NGO
    When I visit the request page
    And I accept a request
    Then I should see the new user of my NGO

  Scenario: Reject a user request
    Given I am a user administrator of an NGO
    Given There are requests for my NGO
    When I visit the request page
    And I reject a request
    Then I should not see the new user of my NGO
