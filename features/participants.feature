Feature: Participants
I should be able to publish and see participants in the app

Background:
  Given I am a registered user
  And I sign in
    Scenario: Listing participants
      Given There are several participants
      And There are several ngos
      And I am admin of the NGO
      When I visit the participants page
      Then I should see a list of participants of that ngo

    Scenario: Changing the name of a participant
      Given There is a participant in the page
      And There are several ngos
      And I am admin of the NGO
      And The participant is related to my NGO
      When I change the name of the participant
      Then I should see the new participant in the list

    @javascript
    Scenario: Removing a participant
      Given There is a participant in the page
      And There are several ngos
      And  I am admin of the NGO
      And  The participant is related to my NGO
      When I delete the participant
      Then I should not see the participant in the list

    Scenario: Reading a participant
      Given There are several ngos
      And There is a participant in a team of my ngo
      And I am an worker of the ngo
      And The user is associated with a project
      When I visit the participants page
      And I select the participant
      Then I should see the participant details

    Scenario: Adding a participant to the team as a worker
      Given There are several ngos
      And There is a participant in a team of my ngo
      And I am an worker of the ngo
      When I add a new participant to a team of the ngo
      Then I should see the new participant in the list
    @javascript
    Scenario: Deleting a participant from a team as a worker
      Given There are several participants
      And There are several ngos
      And I am an worker of the ngo
      And There is a participant in a team of my ngo
      And The user is associated with a project
      When I delete the participant from the team
      Then I should not see the participant in the project list

    Scenario: Changing a participant role in a team as a worker
      Given There are several ngos
      And I am an worker of the ngo
      And There is a participant in a team of my ngo
      And The user is associated with a project
      When I make the participant leader
      Then I should see the participant as a leader in the list

    Scenario: Creating a new participant for a team as a worker
      Given There are several participants
      And There are several ngos
      And I am an worker of the ngo
      And There is a participant in a team of my ngo
      And The user is associated with a project
      When I create a new participant for a team
      Then I should see the new participant in the project list

    Scenario: Seeing participant details as a worker associated to the project
      Given There are several ngos
      And There is a participant in a team of my ngo
      And I am an worker of the ngo
      And The user is associated with a project
      When I visit the participant details page
      Then I should see the participant details

    Scenario: Editing participant details as a worker
      Given There are several ngos
      And There is a participant in a team of my ngo
      And I am an worker of the ngo
      And The user is associated with a project
      When I edit the participant details page
      Then I should see the new participant details

    Scenario: Listing participants as a worker
      Given There are several ngos
      And I am an worker of the ngo
      And There are several participant in a team of my ngo
      When I visit the participants page
      Then I should see the participants associated to my ngo
