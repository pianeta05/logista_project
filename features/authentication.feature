Feature: Authentication

Scenario: Sign up
  Given I am not signed in
  When I sign up
  Then I should see that my account is created

Scenario: Sign in
  Given I am a registered user
  And I am not signed in
  When I sign in
  Then I should be signed in

Scenario: Sign out
  Given I am a registered user
  And I sign in
  When I sign out
  Then I should be signed out

Scenario: Edit my profile
  Given I am a registered user
  And I sign in
  When I edit my profile
  Then I should see the changes

Scenario: Change my password
  Given I am a registered user
  And I sign in
  When I edit my password
  And I sign out
  And I sign in with the new password
  Then I should be signed in

@javascript
Scenario: Cancel my account
  Given I am a registered user
  And I sign in
  When I cancel my account
  Then I should not be able to sign in
