Given("The NGO has a worker") do
  @worker = FactoryBot.create(:user)
  @ngo.add_worker(@worker)
end

Given("There are several projects") do
  Faker::UniqueGenerator.clear
  FactoryBot.create_list(:project, 3, ngo: @ngo)
  @project = Project.first
end

Given("There are some future projects") do
  # FactoryBot has a limited number of unique names
  Faker::UniqueGenerator.clear
  # Make sure there are no more future projects than the list
  Project.destroy_all
  # Create the future and non-future projects lists
  @non_future_projects = [past_project, current_project]
  @future_projects = FactoryBot.create_list(:project, 3, start_day: Date.today + 1.day, final_day: Date.today + 3.day)
  @non_future_projects.each do |project|
    project.update(name: @future_projects.first.name + '2')
  end
  @project = Project.first
end

Given("There are some past projects") do
  # FactoryBot has a limited number of unique names
  Faker::UniqueGenerator.clear
  # Make sure there are no more past projects than the list
  Project.destroy_all
  # Create the future and non-past projects lists
  @non_past_projects = [current_project, future_project]
  @past_projects = FactoryBot.create_list(:project, 3, start_day: Date.today - 5.day, final_day: Date.today - 3.day)
  @non_past_projects.each do |project|
    project.update(name: @past_projects.first.name + '2')
  end
  @project = Project.first
end

Given("There are some current projects") do
  # FactoryBot has a limited number of unique names
  Faker::UniqueGenerator.clear
  # Make sure there are no more current projects than the list
  Project.destroy_all
  # Create the future and non-current projects lists
  @non_current_projects = [past_project, future_project]
  @current_projects = FactoryBot.create_list(:project, 3, start_day: Date.today - 5.day, final_day: Date.today + 3.day)
  @non_current_projects.each do |project|
    project.update(name: @current_projects.first.name + '2')
  end
  @project = Project.first
end

Given("The worker is associated with a project") do
 @project.add_worker(@worker.id)
end

Given("There is a worker") do
  @worker= FactoryBot.create(:user)
  @ngo.add_worker(@worker)
end

Given("I log out as an admin and log in as a worker") do
  click_on "My Account"
  click_on "Sign Out"
  visit sign_in_path
  fill_in "user_email", with: @worker.email
  fill_in "user_password", with: @worker.password
  click_on "Send"
end

Given("The worker is associated with the project") do
  @project.add_worker(@worker.id)
end


When("I add a worker to the project") do
  visit project_path(@project)
  page.find('.check-box.mr-2', text: @worker.name).click_on('Add')
end

When("I visit a project's page") do
  visit project_path(@project)
end

When("I visit the projects page") do
  visit projects_path
end

When("I create a new project") do
  @project = FactoryBot.build(:project, ngo_id:@ngo.id)
  visit new_ngo_project_path(@ngo)
  select @project.category, from: "project_category"
  fill_in 'project_name', with: @project.name
  fill_in 'project_code', with: @project.code
  fill_in 'project_country', with: @project.country
  fill_in 'project[dates]', with: "01/02/2020  03/03/2020"
  fill_in "project_city", with: @project.city

  click_button I18n.t('projects.form.save_button')
end

When("I change the name of the project") do
  @new_name = 'New project name'
  @old_name = @project.name
  visit edit_project_path(@project)
  fill_in 'project[dates]', with: "01-02-2020  03-03-2020"
  fill_in "project_name", with: @new_name
  click_button I18n.t('projects.form.save_button')
end

When("I delete a project") do
  visit projects_path
  page.find('li.list-group-item', text: @project.name).click_on(I18n.t('projects.project.delete'))
  page.driver.browser.switch_to.alert.accept
end

When("I select a project") do
  page.find('li.list-group-item', text: @project.name).click_on(@project.name)
end

When("I search for the future projects") do
  visit projects_path
  click_on I18n.t('projects.project_search_form.future')
end

When("I search for the past projects") do
  visit projects_path
  click_on I18n.t('projects.project_search_form.past')
end

When("I search for the current projects") do
  visit projects_path
  click_on I18n.t('projects.project_search_form.current')
end

Then("I should see a list of projects") do
  Project.all.each do |project|
    expect(page).to have_content(project.name)
  end
end

Then("I should see the new project in the list") do
  visit projects_path
  expect(page).to have_content(@project.name)
end

Then("I should see the new name in the list") do
  visit projects_path
  expect(page).to have_content(@new_name)
  expect(page).not_to have_content(@old_name)
end

Then("I should see the new worker in the project") do
  expect(page.find('.check-box.mr-2', text: @worker.name)).to have_content('Delete')
end

Then("I should not see the project in the list") do
  visit projects_path
  expect(page).to_not have_content(@project.name)
end

Then("I should see the project details") do
  expect(page).to have_content(@project.name)
  expect(page).to have_content(@project.code)
  expect(page).to have_content(@project.country)
  expect(page).to have_content(@project.start_day)
  expect(page).to have_content(@project.final_day)
  expect(page).to have_content(@project.city)
end

Then("I should see the project teams") do
  @project.teams.each do |team|
    expect(page).to have_content(team.country)
  end
end

Then("I should only see the future projects") do
  @future_projects.each do |project|
    expect(page).to have_content(project.name)
  end
  @non_future_projects.each do |project|
    expect(page).not_to have_content(project.name)
  end
end

Then("I should only see the past projects") do
  @past_projects.each do |project|
    expect(page).to have_content(project.name)
  end

  @non_past_projects.each do |project|
    expect(page).not_to have_content(project.name)
  end
end

Then("I should only see the current projects") do
  @current_projects.each do |project|
    expect(page).to have_content(project.name)
  end

  @non_current_projects.each do |project|
    expect(page).not_to have_content(project.name)
  end
end

Given("There are some projects to search") do
  # FactoryBot has a limited number of unique names
  Faker::UniqueGenerator.clear
  # Make sure there are no more current projects than the list
  Project.destroy_all
  @non_in_search_projects = FactoryBot.create_list(:project,9, name: 'a')
  @project_in_search  = FactoryBot.create(:project, name: 'b')
  @project = Project.first
end

When("I search a project") do
  visit projects_path
  fill_in 'query', with: @project_in_search.name
  click_on I18n.t('projects.project_search_form.button_search')
end

Then("I should see the project in the list of search") do
  expect(page.find('#search-results')).to have_content(@project_in_search.name)
  @non_in_search_projects.each do |project|
    expect(page.find('#search-results')).not_to have_content(project.name)
  end
end

When("I search a project by start date") do
  @non_in_search_projects << project_with_different_start_day
  visit projects_path
  fill_in 'query', with: @project_in_search.name
  click_on I18n.t('projects.project_search_form.button_advanced')
  page.find('#start_day').set(@project_in_search.start_day.strftime('%Y-%m-%d'))
  click_on I18n.t('projects.project_search_form.button_search')
end

When("I search a project using all parameters") do
  @non_in_search_projects << project_with_different_city
  @non_in_search_projects << project_with_different_start_day
  @non_in_search_projects << project_with_different_final_day
  visit projects_path
  fill_in "query", with: @project_in_search.name
  click_on I18n.t('projects.project_search_form.button_advanced')
  fill_in "city", with: @project_in_search.city
  page.find('#start_day').set(@project_in_search.start_day.strftime('%Y-%m-%d'))
  page.find('#final_day').set(@project_in_search.final_day.strftime('%Y-%m-%d'))
  click_on I18n.t('projects.project_search_form.button_search')
end


When("I search a project by final day") do
  @non_in_search_projects << project_with_different_final_day
  visit projects_path
  fill_in 'query', with: @project_in_search.name
  click_on I18n.t('projects.project_search_form.button_advanced')
  page.find('#final_day').set(@project_in_search.final_day.strftime('%Y-%m-%d'))
  click_on I18n.t('projects.project_search_form.button_search')
end


When("I search a project by city") do
  @non_in_search_projects << project_with_different_city
  visit projects_path
  fill_in 'query', with: @project_in_search.name
  click_on I18n.t('projects.project_search_form.button_advanced')
  fill_in 'city', with: @project_in_search.city
  click_on I18n.t('projects.project_search_form.button_search')
end


Given("There is a NGO and a project assosiated to the NGO") do
  @ngo = FactoryBot.create(:ngo)
  @project = FactoryBot.create(:project, ngo_id:@ngo.id)
end

Given("I am a worker") do
  @ngo.add_worker(@user)
end

Then("I could see the projects button on the NGO page") do
  visit ngo_path(@ngo)
  expect(page).to have_selector(:link_or_button, 'Projects')
end

When("I sign in as a admin") do
  @ngo.add_admin(@user)
end

When("There is a worker assosiated to the NGO") do
  @worker=FactoryBot.create(:user)
  @ngo.add_worker(@worker)
end

Then("I should add worker to a project") do
  visit project_path(@project)
  page.find('label.check-box.mr-2', text: @worker.name).click_on('Add')
  expect(page).to have_selector('.alert', text: 'Successfully added')
end

Then("I should not able to edit and delete a NGO") do
  visit ngo_path(@ngo)
  expect(page).not_to have_css('.btn.btn-primary', text: 'Edit')
  expect(page).not_to have_css('.btn.btn-danger', text: 'Delete')
end

Then("I should not able to edit and delete a project") do
  visit projects_ngo_path(@ngo)
  expect(page).not_to have_css('.btn.btn-primary', text: 'Edit')
  expect(page).not_to have_css('.btn.btn-danger', text: 'Delete')
end


When("I delete the worker from the project") do
  visit project_path(@project)
  page.find('label.check-box.mr-2', text: @worker.name).click_on('Delete')
end

Then("I should not see the worker in the project") do
  expect(page.find('label.check-box.mr-2', text: @worker.name)).to have_css('a', text:"Add")
end

When("I try to list all project from all ngos") do
  @ngo2 =FactoryBot.create(:ngo)
  @project2 = FactoryBot.create(:project, ngo_id:@ngo2.id)
  visit projects_path
end

Then("I should see the list") do
  @ngo.projects.each do |project|
    expect(page).to have_content(project.name)
  end
  @ngo2.projects.each do |project|
    expect(page).to have_content(project.name)
  end
end


Then("I should be able to see the project details") do
  expect(page).to have_content(@project.name)
  expect(page).to have_content(@project.code)
  expect(page).to have_content(@project.country)
  expect(page).to have_content(@project.start_day)
  expect(page).to have_content(@project.final_day)
  expect(page).to have_content(@project.city)
  expect(page).to have_selector(:link_or_button, I18n.t('projects.show.new'))
end

Then("I should be able to see the project teams list") do
  @project.teams.each do |team|
    expect(page).to have_content(team.country)
  end
end

When("I search projects") do
  visit projects_path
  fill_in 'query', with: @project.name
  click_on I18n.t('projects.project_search_form.button_search')
end

Then("I should see the search results") do
    expect(page).to have_content(@project.name)
end



def project_with_different_city
  FactoryBot.create(:project, name: @project_in_search.name + '2', city: @project_in_search.city + 'aaa', start_day: @project_in_search.start_day, final_day: @project_in_search.final_day)
end

def project_with_different_start_day
  FactoryBot.create(:project, name: @project_in_search.name + '2', city: @project_in_search.city, start_day: @project_in_search.start_day - 1.day, final_day: @project_in_search.final_day)
end

def project_with_different_final_day
  FactoryBot.create(:project, name: @project_in_search.name + '2', city: @project_in_search.city, start_day: @project_in_search.start_day, final_day: @project_in_search.final_day + 1.day)
end

def past_project
  FactoryBot.create(:project,  start_day: Date.today - 5.day, final_day: Date.today - 1.day)
end

def current_project
  FactoryBot.create(:project, start_day: Date.today - 5.day, final_day: Date.today + 1.day)
end

def future_project
  FactoryBot.create(:project, start_day: Date.today + 1.day, final_day: Date.today + 5.day)
end
