Given("I am a registered user") do
  @user = FactoryBot.create(:user)
end

Given("I am not signed in") do
  visit root_path
  if page.has_text?('Sign Out')
      click_on 'Sign Out'
  end
end

# Given("I sign out") do
#   click_on 'Sign Out'
# end

Given("I log in as an admin") do
  visit sign_in_path
  fill_in "user_email", with: @admin.email
  fill_in "user_password", with: @admin.password
  click_on "Log in"
end

When("I cancel my account") do
  visit profile_path
  click_on("Cancel my account")
  page.driver.browser.switch_to.alert.accept
  end

When("I edit my password") do
  visit profile_path
  @new_user_details = FactoryBot.build(:user)
  fill_in "user[password]", with: @new_user_details.password
  fill_in "user[password_confirmation]", with: @new_user_details.password
  fill_in "user[current_password]", with: @user.password
  click_on "Update"
end

When("I edit my profile") do
  visit profile_path
  @new_user_details = FactoryBot.build(:user)
  fill_in "user_email", with: @new_user_details.email
  fill_in "user_name", with: @new_user_details.name
  fill_in "user_current_password", with: @user.password
  click_on "Update"
end

When("I sign in") do
  visit sign_in_path
  fill_in "user_email", with: @user.email
  fill_in "user_password", with: @user.password
  click_on "Send"
end

When("I sign in with the new password") do
  visit sign_in_path
  fill_in "user_email", with: @user.email
  fill_in "user_password", with: @new_user_details.password
  click_on "Send"
end

When("I sign out") do
  click_on "My Account"
  click_on "Sign Out"
end

When("I sign up") do
  @user = FactoryBot.build(:user)
  visit sign_up_path
  fill_in "user[name]", with: @user.name
  fill_in "user[email]", with: @user.email
  fill_in "user[password]", with: @user.password
  fill_in "user[password_confirmation]", with:@user.password
  click_on "Save"
end

Then("I should be signed in") do
  expect(page).to have_content("My Account")
  expect(page).not_to have_content("Sign In")
end

Then("I should be signed out") do
  expect(page).not_to have_content("Sign Out")
  expect(page).to have_content("Sign In")
end

Then("I should not be able to sign in") do
  visit sign_in_path
  fill_in "user_email", with: @user.email
  fill_in "user_password", with: @user.password
  click_on "Send"
  expect(page).to have_content("Invalid Email or password.");
  expect(page).not_to have_content("My Account");
end

Then("I should see that my account is created") do
  expect(page).to have_content("You have signed up successfully")
end

Then("I should see the changes") do
  visit profile_path
  expect(find_field("user_email").value).to eq @new_user_details.email
  expect(find_field("user_name").value).to eq @new_user_details.name
end
