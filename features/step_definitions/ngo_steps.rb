Given("There are several ngos") do
  @ngo1=FactoryBot.create(:ngo, name: "NGO1")
  @ngo2=FactoryBot.create(:ngo, name: "NGO2")
end

Given("I am a worker of the ngo") do
  @ngo1.add_worker(@user)
end

When("I visit the ngos page") do
  visit root_path
  find('li.dropdown', :text => 'NGO').click
end

When("I select create ngo") do
  click_link "New"
end

When("I visit the ngo page") do
  visit ngo_path(@ngo1)
end

When("I visit the ngo projects page") do
  visit ngo_projects_path(@ngo1)
end


Then("I should see a list of ngos") do
  expect(page).to have_content("NGO1")
  expect(page).to have_content("NGO2")
end

Then("I should see the ngo projects") do
  @ngo1.projects.each do |project|
    expect(page).to have_content project.name
  end
end

Then("I should see the ngo users details") do
  @ngo1.users.each do |user|
    expect(page).to have_content user.name
  end
end


When("I submit a new ngo") do
  @ngo1 = FactoryBot.build(:ngo, name: 'Murcia')
  visit my_ngos_path
  click_button 'Create NGO'
  fill_in 'ngo_name', with: @ngo1.name
  fill_in 'ngo_country', with: @ngo1.country
  click_button 'Save'
end

Then("I should see the new ngo in the list") do
  visit my_ngos_path
  expect(page).to have_content(@ngo1.name)
end

Given("There is a ngo in the page") do
  @ngo = FactoryBot.create(:ngo)
end

When("I change the name of the ngo") do
  visit my_ngos_path
  page.find('li.list-group-item', text: @ngo1.name).click_on('Edit')
  fill_in "ngo_name", with: "Madeira"
  click_button "Save"
end

When("I change the name of the ngo created") do
  visit my_ngos_path
  page.find('li.list-group-item', text: @user.name).click_on('Edit')
  fill_in "ngo_name", with: "Madeira"
  click_button "Save"
end

Then("I should see the new name of ngo created in the list") do
  visit my_ngos_path
  expect(page).to have_content("Madeira")
  expect(page).not_to have_content(@user.name)
end

Then("I should see the new name of ngo in the list") do
  visit my_ngos_path
  expect(page).to have_content("Madeira")
  expect(page).not_to have_content(@user.name)
end

When("I delete the ngo") do
  visit root_path
  find('li.dropdown', :text => 'NGO').click
  click_link "My NGOs"
  page.find('li.list-group-item', text: @ngo1.name).click_on('Delete')
  page.driver.browser.switch_to.alert.accept
end

Then("I should not see the ngo in the list") do
  visit root_path
  find('li.dropdown', :text => 'NGO').click
  click_link "My NGOs"
  expect(page).to_not have_content(@ngo1.name)
end

When("I select a ngo") do
  find('li.dropdown', :text => 'NGO').click
  click_link "My NGOs"
  page.find('li.list-group-item', text: @ngo1.name).click_on(@ngo1.name)
end

Then("I should see the ngo details") do
  expect(page).to have_content(@ngo1.name)
  expect(page).to have_content(@ngo1.country)
end

When("I should not see the NGO page") do
  expect(page).to_not have_content(@ngo1.name)
end

Given("I fill the sign in form with other user") do
  @pedro = FactoryBot.create(:user, name:"Pedro")
  visit root_path
  if page.has_text?('Sign out')
      click_on 'Sign out'
  end
  visit sign_in_path
  fill_in "user_email", with: @pedro.email
  fill_in "user_password", with: @pedro.password
  click_on "Send"
end


Given("I am an not admin user") do
  NgoUser.create(ngo_id:@ngo1.id, user_id: @user.id, admin:false)
end

Given("I am an admin user") do
  NgoUser.create(ngo_id:@ngo.id, user_id: @user.id, admin:true)
end

When("I Try to edit the NGO") do
  visit edit_ngo_path(@ngo1.id)
end

Then("I see an error message") do
  expect(page).to have_content("Only Admins can see this page.")
end

Given("I am the admin of the NGO") do
  NgoUser.create(ngo_id:@ngo1.id, user_id: @user.id, admin:true)
end

Given("There is another admin of the NGO") do
  @admin = FactoryBot.create(:user)
  NgoUser.create(ngo_id:@ngo1.id, user_id: @admin.id, admin:true)
end

When("I visit the NGO") do
  visit ngo_path(@ngo1.id)
end

When("I transform an admin to worker") do
 page.find('li.list-group-item', text: @admin.name).click_on I18n.t('ngos.user.make_worker_button')
 page.driver.browser.switch_to.alert.accept
end

Then("I should not see the admin in the NGO") do
  expect(page.find('li.list-group-item', text:@admin.name)).to have_content I18n.t('ngos.user.make_admin_button')
end

Given("There is a worker in the NGO") do
  @worker = FactoryBot.create(:user)
  NgoUser.create(ngo_id:@ngo1.id, user_id: @worker.id, admin:false)
end

When("I transform a worker to admin") do
  page.find('li.list-group-item', text: @worker.name).click_on I18n.t('ngos.user.make_admin_button')
end

Then("I should see the new ngo's admin") do
  expect(page.find('li.list-group-item', text:@worker.name)).to have_content I18n.t('ngos.user.make_worker_button')
end

Given("There is another user in the NGO") do
  @user_in_ngo = FactoryBot.create(:user)
  NgoUser.create(ngo_id: @ngo1.id, user_id: @user_in_ngo.id, admin:false)
end

When("I delete a user") do
  page.find('li.list-group-item', text: @user_in_ngo.name).click_on I18n.t('ngos.user.delete_button')
  page.driver.browser.switch_to.alert.accept
end

Then("I should not see the user deleted on the page") do
  expect(page.find('li.list-group-item')).to_not have_content(@user_in_ngo.name)
end

When("I try to make me worker to the last admin") do
  page.find('li.list-group-item', text: @user.name).click_on I18n.t('ngos.user.make_worker_button')
  page.driver.browser.switch_to.alert.accept
end

Then("I still need to be admin of this NGO") do
  expect(page.find('li.list-group-item', text:@user.name)).to have_selector(:link_or_button, "Make worker")
end

When("I delete the last admin on the NGO") do
  page.find('li.list-group-item', text: @user.name).click_on I18n.t('ngos.user.delete_button')
  page.driver.browser.switch_to.alert.accept
end
