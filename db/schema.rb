# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_07_131808) do

  create_table "ngo_users", force: :cascade do |t|
    t.integer "user_id"
    t.integer "ngo_id"
    t.boolean "admin"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ngo_id"], name: "index_ngo_users_on_ngo_id"
    t.index ["user_id"], name: "index_ngo_users_on_user_id"
  end

  create_table "ngos", force: :cascade do |t|
    t.string "name"
    t.string "country"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "participant_teams", force: :cascade do |t|
    t.integer "participant_id"
    t.integer "team_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "leader"
    t.index ["participant_id"], name: "index_participant_teams_on_participant_id"
    t.index ["team_id"], name: "index_participant_teams_on_team_id"
  end

  create_table "participants", force: :cascade do |t|
    t.string "name"
    t.string "nationality"
    t.string "residence"
    t.integer "telephone"
    t.string "email"
    t.date "date_of_birth"
    t.integer "project_id"
    t.integer "team_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "sex"
    t.string "country_code"
    t.index ["project_id"], name: "index_participants_on_project_id"
    t.index ["team_id"], name: "index_participants_on_team_id"
  end

  create_table "project_users", force: :cascade do |t|
    t.integer "project_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_project_users_on_project_id"
    t.index ["user_id"], name: "index_project_users_on_user_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string "name"
    t.date "start_day"
    t.date "final_day"
    t.string "city"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "category"
    t.string "code"
    t.string "country"
    t.integer "ngo_id"
  end

  create_table "requests", force: :cascade do |t|
    t.integer "user_id"
    t.integer "ngo_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ngo_id"], name: "index_requests_on_ngo_id"
    t.index ["user_id"], name: "index_requests_on_user_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "country"
    t.float "budget"
    t.integer "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_teams_on_project_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
