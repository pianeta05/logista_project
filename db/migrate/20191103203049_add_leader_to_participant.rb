class AddLeaderToParticipant < ActiveRecord::Migration[5.2]
  def change
    add_column :participants, :leader, :boolean
  end
end
