class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :name
      t.date :start_day
      t.date :final_day
      t.string :city

      t.timestamps
    end
  end
end
