class CreateParticipants < ActiveRecord::Migration[5.2]
  def change
    create_table :participants do |t|
      t.string :name
      t.string :nationality
      t.string :residence
      t.integer :telephone
      t.string :email
      t.date :date_of_birth
      t.references :project, foreign_key: true
      t.references :team, foreign_key: true

      t.timestamps
    end
  end
end
