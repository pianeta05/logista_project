
class ChangeAdminToBeBooleanInNgoUser < ActiveRecord::Migration[5.2]
  def up
    change_column :ngo_users, :admin, :boolean
  end

  def down
    change_column :ngo_users, :admin, :float
  end
end
