class AddCountryToProject < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :country, :string
  end
end
