class AddNoAdminToNgoUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :ngo_users, :no_admin, :boolean
  end
end
