class CreateParticipantTeams < ActiveRecord::Migration[5.2]
  def change
    create_table :participant_teams do |t|
      t.references :participant, foreign_key: true
      t.references :team, foreign_key: true

      t.timestamps
    end
  end
end
