class AddSexToParticipant < ActiveRecord::Migration[5.2]
  def change
    add_column :participants, :sex, :string
  end
end
