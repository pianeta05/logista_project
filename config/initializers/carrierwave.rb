if Rails.env.production?
  CarrierWave.configure do |config|
    config.fog_credentials = {
      aws_access_key_id: ENV['S3_KEY_ID'],
      aws_secret_access_key: ENV['S3_SECRET_KEY'],
      provider: 'AWS',
      region: 'eu-west-2'
    }
    config.fog_directory = 'fotologcarlosbucket'
  end
end
