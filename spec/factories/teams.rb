# == Schema Information
#
# Table name: teams
#
#  id         :integer          not null, primary key
#  country    :string
#  budget     :float
#  project_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :team do
    country   { Faker::Address.unique.country.titleize  }
    budget    { Faker::Number.decimal(l_digits: 3, r_digits: 2).to_f }
    project
  end

  factory :invalid_team, class: 'Team' do
    country       {""}
    budget        {-1}
    project       {nil}
  end
end
