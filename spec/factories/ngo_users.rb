# == Schema Information
#
# Table name: ngo_users
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  ngo_id     :integer
#  admin      :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :ngo_user do
    user
    ngo   
    admin { Faker::Boolean.boolean }
  end
end
