# == Schema Information
#
# Table name: ngos
#
#  id         :integer          not null, primary key
#  name       :string
#  country    :string
#  image      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :ngo do
    name    { Faker::Educator.subject }
    country { Faker::Address.country }
    #Use carrierwave with FactoryBot
    image { Rack::Test::UploadedFile.new(Rails.root.join('spec/support/images.jpeg'), 'image/jpeg') }
  end

  factory :invalid_ngo, class: 'Ngo' do
    name    { '' }
    country { '' }
    image   { '' }
  end

  # It sends the date range in the same way the form would
  factory :ngo_from_form, class: 'Ngo' do
    name      {Faker::Educator.subject}
    country   {Faker::Address.country}
    image   { Faker::Avatar.image  }
  end

  factory :invalid_ngo_from_form, class: 'Ngo' do
    name           {""}
    country        {""}
    image          {""}
  end
end
