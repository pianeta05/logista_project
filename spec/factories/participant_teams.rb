# == Schema Information
#
# Table name: participant_teams
#
#  id             :integer          not null, primary key
#  participant_id :integer
#  team_id        :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  leader         :boolean
#

FactoryBot.define do
  factory :participant_team do
    participant { nil }
    team { nil }
  end
end
