# == Schema Information
#
# Table name: teams
#
#  id         :integer          not null, primary key
#  country    :string
#  budget     :float
#  project_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Team, type: :model do
  it { is_expected.to have_db_column(:country).of_type(:string) }
  it { is_expected.to have_db_column(:budget).of_type(:float) }

  it { is_expected.to validate_presence_of(:country) }
  it { is_expected.to validate_length_of(:country).is_at_most(100) }
  it { is_expected.to validate_numericality_of(:budget).is_greater_than_or_equal_to(0).with_message("No puede ser negativo") }
end
