# == Schema Information
#
# Table name: projects
#
#  id         :integer          not null, primary key
#  name       :string
#  start_day  :date
#  final_day  :date
#  city       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  category   :string
#  code       :string
#  country    :string
#  ngo_id     :integer
#

require 'rails_helper'

RSpec.describe Project, type: :model do
  it { is_expected.to have_db_column(:name).of_type(:string)}
  it { is_expected.to have_db_column(:start_day).of_type(:date)}
  it { is_expected.to have_db_column(:final_day).of_type(:date)}
  it { is_expected.to have_db_column(:city).of_type(:string)}
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:city) }
  it { is_expected.to validate_presence_of(:code) }
  it { is_expected.to validate_presence_of(:country) }
  it { is_expected.to validate_length_of(:name).is_at_most(200) }
  it { is_expected.to validate_length_of(:city).is_at_most(100) }
  it { is_expected.to validate_length_of(:country).is_at_most(100) }

  it "should not admit final dates later than start dates" do
    project = FactoryBot.build(:project, start_day: Date.today, final_day: Date.today - 1.day)
    expect(project).not_to be_valid
  end

  describe "Scopes" do

    before(:all) do
      @current = FactoryBot.create(:project, start_day: Date.today - 1.day, final_day: Date.today + 1.day)
      @past = FactoryBot.create(:project, start_day: Date.today - 5.day, final_day: Date.today - 1.day)
      @future = FactoryBot.create(:project, start_day: Date.today + 1.day, final_day: Date.today + 5.day)
    end

    it "should find current projects" do
      expect(Project.current).to include(@current)
      expect(Project.current).not_to include(@past, @future)
    end

    it "should find past projects" do
      expect(Project.past).to include(@past)
      expect(Project.past).not_to include(@current, @future)
    end

    it "should find future projects" do
      expect(Project.future).to include(@future)
      expect(Project.future).not_to include(@current, @past)
    end
  end
  describe "Methods" do
    before (:all) do
      @worker = FactoryBot.create(:user)
      @ngo = FactoryBot.create(:ngo)
      @project = FactoryBot.create(:project, ngo: @ngo)
    end

    describe 'has_worker' do
      #When there is not assotiation, it must be 0
      it 'should return false when there are no workers' do
        @ngo.add_worker(@worker)
        expect(@project.has_worker?(@worker.id)).to be false
      end

      #When there is assotiation, it must be 1
      it "should return true when the worker is in the project" do
        @ngo.add_worker(@worker)
        @project.add_worker(@worker.id)
        expect(@project.has_worker?(@worker.id)).to be true
      end
    end

    describe 'add_worker' do
      #When the params passed is worker and it is not assotiated
      it "should associate a new worker when all requirements are met" do
        @ngo.add_worker(@worker)
        expect {
          @project.add_worker(@worker.id)
        }.to change{@project.project_users.count}.by(1)
      end

      #When the params passed is admin
      it "should not do anything when receiving an admin" do
        @project.ngo.add_admin(@worker)
        @project.add_worker(@worker.id)
        expect(@project.project_users.count).to eq(0)
      end

      #When the params passed is worker and it is already assotiated
      it "should not do anything when receiving a worker already in the project" do
        @ngo.add_worker(@worker)
        @project.add_worker(@worker.id)
        @project.add_worker(@worker.id)
        expect(@project.project_users.count).to eq(1)
      end

      it "should not do anything when receiving a worker that is not associated to the project ngo" do
        expect{
          @project.add_worker(@worker.id)
        }.not_to change{@project.project_users.count}
      end
    end

    describe 'can_edit?' do
      before :each do
        @ngo = FactoryBot.create(:ngo)
        @project = FactoryBot.create(:project, ngo: @ngo)
        @user = FactoryBot.create(:user)
      end
      it 'should return true if the user is a NGO admin' do
        @ngo.add_admin(@user)
        expect(@project.can_edit?(@user)).to be true
      end

      it 'should return true if the user is not admin but is a worker of the project' do
        @ngo.add_worker(@user)
        @project.add_worker(@user.id)
        expect(@project.can_edit?(@user)).to be true
      end

      it 'should return false if the user is a worker of the ngo but is not associated to the project' do
        @ngo.add_worker(@user)
        expect(@project.can_edit?(@user)).to be false
      end

      it 'should return false if the user is not associated to the ngo' do
        expect(@project.can_edit?(@user)).to be false
      end
    end
  end

end
