# == Schema Information
#
# Table name: participants
#
#  id            :integer          not null, primary key
#  name          :string
#  nationality   :string
#  residence     :string
#  telephone     :integer
#  email         :string
#  date_of_birth :date
#  project_id    :integer
#  team_id       :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  sex           :string
#  country_code  :string
#

require 'rails_helper'

RSpec.describe Participant, type: :model do
  it { is_expected.to have_db_column(:name).of_type(:string) }
  it { is_expected.to have_db_column(:nationality).of_type(:string) }
  it { is_expected.to have_db_column(:residence).of_type(:string) }
  it { is_expected.to have_db_column(:telephone).of_type(:integer) }
  it { is_expected.to have_db_column(:email).of_type(:string) }
  it { is_expected.to have_db_column(:date_of_birth).of_type(:date) }
  it { is_expected.to have_db_column(:sex).of_type(:string) }


  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:nationality) }
  it { is_expected.to validate_presence_of(:residence) }
  it { is_expected.to validate_presence_of(:date_of_birth) }
  it { is_expected.to validate_presence_of(:sex) }
  it { is_expected.to validate_length_of(:name).is_at_most(200) }
  it { is_expected.to validate_length_of(:nationality).is_at_most(100) }
  it { is_expected.to validate_length_of(:residence).is_at_most(100) }
  it { is_expected.to validate_length_of(:email).is_at_most(100) }

  describe "Methods" do
    before(:all) do
      @participant = FactoryBot.create(:participant)
    end

    describe :ngos do
      it "returns an empty relation when the participant is not in any team" do
        expect(@participant.ngos).to be_empty
      end

      it "returns a relation with the team's NGO if the participant is in one team" do
        team = FactoryBot.create(:team)
        team.add_participant(@participant)
        ngo = team.project.ngo
        expect(@participant.ngos).to include(ngo)
        expect(@participant.ngos.count).to eq(1)
      end

      it "returns a relation with both NGOs if the participant is in two teams from different NGOs" do
        team1 = FactoryBot.create(:team)
        team1.add_participant(@participant)
        ngo1 = team1.project.ngo
        team2 = FactoryBot.create(:team)
        team2.add_participant(@participant)
        ngo2 = team2.project.ngo
        expect(@participant.ngos).to include(ngo1)
        expect(@participant.ngos).to include(ngo2)
      end

      it "returns a relation the team's NGO if the participant is in two teams from the same NGO" do
        team1 = FactoryBot.create(:team)
        team1.add_participant(@participant)
        ngo = team1.project.ngo
        project = FactoryBot.create(:project, ngo: ngo)
        team2 = FactoryBot.create(:team, project: project)
        team2.add_participant(@participant)
        expect(@participant.ngos).to include(ngo)
        expect(@participant.ngos.count).to eq(1)
      end
    end

    describe :can_edit? do
      before :each do
        @worker = FactoryBot.create(:user)
        @ngo = FactoryBot.create(:ngo)
        @ngo.add_worker(@worker)
        @project = FactoryBot.create(:project, ngo: @ngo)
        @team= FactoryBot.create(:team, project: @project)
      end

      # When I want to check if user logged can edit a participant, it return true
      it 'returns true when user is associated to the project' do
        @project.add_worker(@worker.id)
        @team.add_participant(@participant)
        expect(@participant.can_edit?(@worker)).to eq(true)
      end

      it 'returns false when user is not associated to the project' do
        @team.add_participant(@participant)
        expect(@participant.can_edit?(@worker)).to eq(false)
      end
    end

    describe :projects do
      it "returns all the projects in whitch the participant is" do

      end

      it "return an empty array if it not in any project " do

      end

    end
  end
end
