require 'rails_helper'

RSpec.feature "Project index", :type => :feature do
  before(:each) do
    Faker::UniqueGenerator.clear
    FactoryBot.create_list(:project, 3)
  end

  scenario 'when the user is not signed in' do
    visit projects_path
    Project.all.each do |project|
      expect(page).to have_content(project.name)
    end
  end

  scenario 'when there are no projects' do
    Project.destroy_all
    visit projects_path
    expect(page).to have_content(I18n.t('projects.index.empty_message'))
  end
end
