require 'rails_helper'

RSpec.feature "Project as a worker", :type => :feature do
  before (:each) do
    create_worker
    @ngo = FactoryBot.create(:ngo)
    @ngo.add_worker(@worker)
    sign_in @worker
    @project = FactoryBot.create(:project, ngo_id:@ngo.id)
  end
  scenario "Trying to edit and delete project I'm not associated to" do
    visit ngo_path(@ngo)
    expect(page).not_to have_css('.btn.btn-primary', text: 'Edit')
    expect(page).not_to have_css('.btn.btn-danger', text: 'Delete')
  end

  scenario "Trying to edit or delete a project I'm associated to" do
    @project.add_worker(@worker.id)
    visit ngo_path(@ngo)
    expect(page).not_to have_css('.btn.btn-primary', text: 'Edit')
    expect(page).not_to have_css('.btn.btn-danger', text: 'Delete')
  end

  def create_worker
    @worker=FactoryBot.create(:user)
  end

end
