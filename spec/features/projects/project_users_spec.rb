require 'rails_helper'

RSpec.feature "Project Users", :type => :feature do
  before (:each) do
    @user=FactoryBot.create(:user)
    @ngo = FactoryBot.create(:ngo)
    @project = FactoryBot.create(:project, ngo_id:@ngo.id)
  end

  scenario 'creating a team with a worker not assigned' do
    create_worker
    @ngo.add_worker(@worker)
    sign_in @worker
    visit new_team_path(project_id:@project.id)
    expect(page).to have_selector('.alert', text: "You're not authorized.")
  end

  scenario 'creating a team with an assigned worker' do
    create_worker
    @ngo.add_worker(@worker)
    sign_in @worker
    ProjectUser.create(project_id: @project.id, user_id: @worker.id)
    @team = FactoryBot.build(:team)
    visit project_path(@project)
    click_on "New Team"
    fill_in "team_country", with: @team.country
    fill_in "team_budget", with: @team.budget
    click_on "Save"
    visit project_path(@project)
    expect(page).to have_content(@team.country)
  end
end
def create_worker
  @worker=FactoryBot.create(:user)
end
