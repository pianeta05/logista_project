require 'rails_helper'

RSpec.feature "Project update", :type => :feature do
  before(:each) do
    @project = FactoryBot.create(:project)
  end

  scenario 'when the user is not an admin' do
    sign_in FactoryBot.create(:user)
    visit projects_path
    expect(page.find('li.list-group-item', text: @project.name)).not_to have_content(I18n.t('projects.project.edit'))
  end
end