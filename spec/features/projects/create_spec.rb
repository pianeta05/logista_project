require 'rails_helper'

RSpec.feature "Project create", :type => :feature do
  before(:each) do
    @ngo = FactoryBot.create(:ngo)
  end

  scenario 'when the user is not an admin' do
    sign_in FactoryBot.create(:user)
    visit ngo_path(@ngo)
    expect(page).not_to have_content(I18n.t('ngos.show.new_project'))
  end

  scenario 'create invalid project' do
    admin = FactoryBot.create(:user)
    @ngo.add_admin(admin)
    sign_in admin
    visit new_ngo_project_path(@ngo)
    click_on I18n.t('projects.form.save_button')
    expect(page).to have_content("Name can't be blank")
  end
end
