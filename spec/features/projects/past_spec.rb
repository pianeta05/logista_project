require 'rails_helper'

RSpec.feature "Project past", :type => :feature do
  before(:each) do
    Project.destroy_all
    Faker::UniqueGenerator.clear
    @non_past_projects = FactoryBot.create_list(:project, 3, start_day: Date.today - 1.day, final_day: Date.today + 1.day, name: 'project2')
    @past_projects = FactoryBot.create_list(:project, 3, start_day: Date.today - 5.day, final_day: Date.today - 1.day, name: 'project')
  end

  scenario 'when the user is not signed in' do
    visit projects_path
    click_on I18n.t('projects.project_search_form.past')
    @past_projects.each do |project|
      expect(page).to have_content(project.name)
    end
    @non_past_projects.each do |project|
      expect(page).not_to have_content(project.name)
    end
  end

  scenario 'when there are no past projects' do
    @past_projects.each do |project|
      project.destroy
    end
    visit projects_path
    click_on I18n.t('projects.project_search_form.past')
    expect(page).to have_content(I18n.t('projects.index.empty_message'))
  end
end
