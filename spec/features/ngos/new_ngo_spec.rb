require 'rails_helper'

RSpec.feature "Ngo create", :type => :feature do
  scenario 'creating ngo without image' do
    sign_in FactoryBot.create(:user)
    @ngo = FactoryBot.build(:ngo)
    visit new_ngo_path
    fill_in 'ngo_name', with: @ngo.name
    fill_in 'ngo_country', with: @ngo.country
    click_on 'Save'
    expect(page).to have_css('img')
  end
end
