require 'rails_helper'

RSpec.feature "Ngo as a worker", :type => :feature do
  before (:each) do
    @worker=FactoryBot.create(:user)
    @ngo = FactoryBot.create(:ngo)
    sign_in @worker
    @ngo.add_worker(@worker)
  end
  scenario 'Trying to edit the NGO' do
    visit my_ngos_path
    expect(page.find('li.list-group-item', text: @ngo.name)).not_to have_selector(:link_or_button, 'Edit')
  end
  scenario 'Trying to delete the NGO' do
    visit my_ngos_path
    expect(page.find('li.list-group-item', text: @ngo.name)).not_to have_selector(:link_or_button, 'Delete')
  end

  scenario 'Trying to delete a NGO user' do
    create_admin
    visit ngo_path(@ngo)
    expect(page.find('li.list-group-item', text:@admin.name)).not_to have_selector(:link_or_button, 'Delete of this Ngo')
  end

  scenario 'Trying to change a user role' do
    create_admin
    visit ngo_path(@ngo)
    expect(page.find('li.list-group-item', text:@admin.name)).not_to have_selector(:link_or_button, 'Make worker')
  end

  def create_admin
    @admin = FactoryBot.create(:user)
    @ngo.add_admin(@admin)
  end
end
