require 'rails_helper'

RSpec.feature "Deleting Participants", :type => :feature do
  before (:each) do
    @worker=FactoryBot.create(:user)
    @ngo = FactoryBot.create(:ngo)
    @ngo.add_worker(@worker)
    sign_in @worker
    @project = FactoryBot.create(:project, ngo_id:@ngo.id)
    @team = FactoryBot.create(:team, project_id:@project.id)
    @participant = FactoryBot.create(:participant)
    @team.add_participant(@participant)
  end

  scenario "Trying to delete participant I'm not associated to" do
    sign_out @worker
    @worker_not_associated=FactoryBot.create(:user)
    @ngo.add_worker(@worker_not_associated)
    sign_in @worker_not_associated
    visit my_ngos_path
    click_on @ngo.name
    click_on I18n.t('layouts.header.participants')
    expect(page.find('li.list-group-item', text: @participant.name)).to_not have_content('Delete')
  end

  scenario "Trying to create a participant for a project I'm not associated to" do
    team = FactoryBot.create(:team)
    visit ngo_new_participant_path(@ngo1, team_id: team.id)
    expect(page).to have_content("You're not authorized")
  end

  scenario "Listing participant not associated to my NGO" do
    sign_out @worker
    @worker_not_associated=FactoryBot.create(:user)
    @ngo.add_worker(@worker_not_associated)
    sign_in @worker_not_associated
    visit ngo_participant_path(@ngo.id, @participant)
    expect(page).to have_content("You're not authorized")
  end

  scenario "Reading participant with a worker not associated to the project" do
    visit ngo_participant_path(@ngo.id, @participant)
    expect(page).to have_content("You're not authorized")
  end

  scenario "Editing a participant with a worker not associated to the project" do
    visit edit_ngo_participant_path(@ngo.id, @participant)
    expect(page).to have_content("You're not authorized")
  end
  #
  # scenario "Listing participants as a worker not associatedto the project" do
  #   sign_out @worker
  #   @worker_not_associated=FactoryBot.create(:user)
  #   @ngo.add_worker(@worker_not_associated)
  #   sign_in @worker_not_associated
  #   byebug
  #   visit ngo_path(@ngo)
  #   click_on I18n.t('layouts.header.participants')
  #   expect(page).to have_content(@participant.name)
  #   expect(page.find('.list-group-item', text: participant.name)).to_not have_content(I18n.t('teams.show.buttons.delete'))
  # end

end
