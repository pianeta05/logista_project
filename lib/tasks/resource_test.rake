desc 'Run all tests for a resource'
task :resource_test, [:resource_name] do |task, args|
  if args[:resource_name]
    resource = args[:resource_name]
    puts "Running tests for #{resource}..."

    puts "Controller test:"
    system "rspec spec/controllers/#{resource}s_controller_spec.rb"

    puts "Model test:"
    system "rspec spec/models/#{resource}_spec.rb"

    puts "Helper test:"
    system "rspec spec/helpers/#{resource}s_helper_spec.rb"

    puts "Error and exception test:"
    system "rspec spec/features/#{resource}s/*.rb"

    puts "Happy path test:"
    system "cucumber features/#{resource}s.feature"
  else
    puts 'You have to define a resource name. For example: rails resource_test[project]'
  end
end
