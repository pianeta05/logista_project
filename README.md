# README

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://bitbucket.org/pianeta05/logista_project/src/master/)

Logista is an application to manage budgets of subsidized projects focused on young people and youth workers. Once these projects have been created, the software allows organizing teams with participants from different countries, managing important information such as train / bus tickets, boarding passes, receipts and taxi bills for these (participants). In order to keep an automated control to generate statistics related to finance, racial diversity etc.
This is an open source project, made with Ruby on Rails..
OPEN TO ANYONE WHO WANTS TO CONTRIBUTE.

### Project Online in Production
Feel free to visit the project online on http://logista-project-asun.herokuapp.com/
Try to Sing UP, create a new project and take a look at the application before you start collaborating.

## Contributing
We welcome contributions, and are especially interested in welcoming first time contributors. We especially welcome contributions from people belonging to groups under-represented in free and open source software!.

##### Basic Knowledges for Developers
Help improve Logista software!of. If you have used one or more of the following techs, then you are in the right place:
    - Ruby On rails
    - HTML / HAML
    - Bootstrap / CSS
###### Consider the following points to solve ISSUES and do a PULL REQUEST:
##### Issues
1. Find logista_project issues https://bitbucket.org/pianeta05/logista_project/issues
2. Choose the issue you want to work on.
3. Claim the issue: Comment below. If someone else has claimed it, ask if they've opened a pull request already and if they're stuck -- maybe you can help them solve a problem or move it along!
4. Update the code with the following command:
```sh
    $ git fetch origin branch name
```
This could bring conflicts between the cloud code and the computer code ... To correct this we do the following:
```sh
    $ git reset --hard origin/branch name
```
5.  Create a new branch within another branch to solve an ISSUE, with an easy-to-read name.. For Example:
```sh
    $ git co -b issue2-make-user-list-longer
```
6.  Test the application to verify that nothing has been broken.
```sh
    $ cucumber
```
7.  When the Issue is fixed and and all the tests pass sucessfully, send a commit from that branch... For Example:
```sh
    $ git add -A
    $ git commit -m “Make the user list longer”
    $ git push origin issue2-make-user-list-longer
```

#### Pull Request
Before doing a pull request, verify that the issue is already fixed and that all cucumber tests pass sucessfully.

1.  Visit the project page in bitbucket and create your pull request.
2.  From "YOUR ISSUE BRANCH" to "SECUNDARY BRANCH"....NEVER TO THE MASTER.
3.  Choose the title. For example:
    Issue#2 making user list longer
4. Try to explain what you did and how you did it in the description.
5. Create pull request.

# Pre requirements

  - Ruby '2.6.x'/ Rails 5.2.x
  - HTML/HAML
  - Bootstrap 4.x and CSS3
  - PostgreSQL
  - Ubuntu 18.04


# Instalation
### Standard Installation
1.  In the console, download a copy of your forked repo with  git clone https://your_username@bitbucket.org/your_username/logista_project.git
2.  Enter the new logista_project directory with cd logista_project
3.  Steps to install gems:
    You may need to first run bundle install if you have older gems in your environment from previous Rails work. If you get an error message like Your Ruby version is 2.x.x, but your Gemfile specified 2.6.1 then you need to install the ruby version 2.6.1 using rvm or rbenv.
        -   Using rvm: rvm install 2.6.1 followed by rvm use 2.6.1
        -   Using rbenv: rbenv install 2.6.1 followed by rbenv local 2.6.1
4.  Run rails db:setup to set up the database

### Next Steps
#### Optional
##### If you do not have a machine configured, we invite you to follow the step by step so that you create one of this:

    Step 1

   - Install RVM (Ruby Version Manager):
```sh
       $  gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 \
7D2BAF1CF37B13E2069D6956105BD0E739499BDB
```
  - Install the rvm stable version by running the command below:
```sh   
 $ curl -sSL https://get.rvm.io | bash -s stable --ruby
```
The command will automatically install packages required, and install the latest stable rvm version.

After the installation is complete, run the following command:
```sh
     $ source /usr/local/rvm/scripts/rvm
```
Now you can use the rvm command to manage the ruby version:
```sh    
   $ rvm version
```
    Step 2
- Setup Ruby Latest Version
```sh   
    $ rvm get stable --autolibs=enable
    usermod -a -G rvm root
```
Now check all available ruby versions:
   ```sh
    $ rvm list known
```
And you will get a lot of available versions of ruby - install the latest stable version Ruby 2.6.1 using the rvm command as shown below.
 ```sh
    $ rvm install ruby-2.6.1
 ```   
After all installation is complete, make the ruby 2.5.1 as a default version on the Ubuntu system.
```sh
    $ rvm --default use ruby-2.6.1
```
Check the Ruby version.
```sh
    $ ruby -v
```
Now you will see ruby 2.6.1 is default ruby version on the Ubuntu 18.04 system.

    Step 3 - Install Nodejs
Ruby on Rails requires a JavaScript runtime to compile the Rails asset pipeline. And for the Rails development on Ubuntu Linux, it's best to install and using Nodejs as the Javascript runtime.

Add the nodejs nodesource repository to the system:
```sh
    $ curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
```
Install the latest version nodejs 10 and some additional packages using the apt command below.
```sh
    $ sudo apt install -y nodejs
    $ sudo apt install gcc g++ make
```
    Step 4 - Configure Ruby Gem

RubyGems is a Ruby Package Manager, coming with the gem command-line tool. It's automatically installed when we install Ruby on the system.

Update gem to the latest version and check it.
```sh
    $ gem update --system
    $ gem -v
```
Note:

This is optional, we can disable gem to install documentation on every ruby package installation. Simply by adding the configuration to the '.gemrc' configuration file.
```sh
    $ echo "gem: --no-document" >> ~/.gemrc
```
    Step 5 - Install Ruby on Rails
In this tutorial, we will be using the latest stable Ruby on Rails 5.2.0. We will install Rails using the gem ruby package manager.

Install Ruby on Rails 5.2.0 using the command below.
```sh
$ gem install rails -v 5.2.0
```
After the installation is complete, check the rails version.
```sh
$ rails -v
```
    Step 6 - Setup PostgreSQL Database for Rails Development

By default, Ruby on Rails is using the SQLite database. It supports many databases system, including MySQL, SQLite, and PostgreSQL. And for this guide, we will be using PostgreSQL.

Install the PostgreSQL database using the apt command below.
```sh
    $ sudo apt install postgresql postgresql-contrib libpq-dev -y
```
After all installation is complete, start the Postgres service and enable it to launch everytime at system boot.
```sh
    $ systemctl start postgresql
    $ systemctl enable postgresql
```
Next, we will configure a password for the Postgres user, and create a new user for the Rails installation.

Login to the 'postgres' user and run the Postgres shell.
```sh
    $ su - postgres
    $ psql
```
Change the Postgres password using the query below.
```sh
    $ \password postgres
```
Type your password and the password for postgres user has been added.

Now we will create a new role for our rails installation. We will create a new role named 'rails_dev' with the privilege of creating the database and with the password 'aqwe123'.

Run the Postgres query below.

create role rails_dev with createdb login password 'aqwe123';
Now check all available roles on the system.
```sh
    $ \du
```
